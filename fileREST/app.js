const express = require('express');
const cors = require('cors');
const routes = require('./routes/routes');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const whitelist = ["http://localhost:3000"]
const corsOptions = {
    origin: (origin, callback) => {
        if (!origin || whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error("Not allowed by CORS"))
        }
    },
    credentials: true,
};

const swaggerOptions = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Mi API',
            version: '1.0.0',
            description: 'Esta es una API simple creada con Express y documentada con Swagger'
        },
        servers: [
            {
                url: 'http://localhost:3030'
            }
        ]
    },
    apis: ['./routes/routes.js']
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

const app = express();
app.use(cors());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
app.use('/', routes);


app.listen(5000, () => console.log('Servidor corriendo en el puerto 5000'));