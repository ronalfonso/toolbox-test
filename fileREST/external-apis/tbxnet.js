const axios = require('axios');
const csvtojson = require('csvtojson');
const {isNumber, is32DigitHex, isNotEmpty} = require('../validators/types');

async function getFiles() {
    const response = await axios.get('https://echo-serv.tbxnet.com/v1/secret/files', {
        headers: {'Authorization': 'Bearer aSuperSecretKey', 'content-type': 'application/json'}
    });

    return response.data.files;
}

async function getFileData(file) {
    const response = await axios.get(`https://echo-serv.tbxnet.com/v1/secret/file/${file}`, {
        headers: {'Authorization': 'Bearer aSuperSecretKey', 'content-type': 'application/json'},
        responseType: 'stream'
    }).then(resp => resp)
        .catch(err => {
            return err.response;
        });

    if (response !== undefined) {
        const json = await csvtojson().fromStream(response.data);
        if (response.status === 500) {
            return {
                file: file,
                lines: [],
                error: true
            }
        }
        if (json.length > 0) {
            return {
                file: json[0].file,
                lines: json
                    .filter(row => isNotEmpty(row.text) && isNotEmpty(row.number) && isNotEmpty(row.hex) && isNumber(row.number) && is32DigitHex(row.hex))
                    .map(row => ({
                        text: row.text,
                        number: row.number,
                        hex: row.hex
                    }))
            };
        }
    }
}

module.exports = {getFiles, getFileData};