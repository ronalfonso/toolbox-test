const express = require('express');
const router = express.Router();
const apiExternal = require('../external-apis/tbxnet');

/**
 * @swagger
 * /files/data:
 *  get:
 *    description: Usa para obtener los datos de los archivos
 *    parameters:
 *      - name: fileName
 *        description: Filtro opcional para los nombres de los archivos
 *        in: query
 *        required: false
 *        type: string
 *    responses:
 *      '200':
 *        description: Una respuesta exitosa
 */
router.get('/files/data', async (req, res) => {
    try {
        const files = await apiExternal.getFiles();
        const results = [];
        for (const file of files) {
            const fileData = await apiExternal.getFileData(file);
            if (fileData !== undefined) results.push(fileData);
        }

        if (req.query.fileName) {
            const filteredResults = results.filter(result => result.file.includes(req.query.fileName));

            res.json(filteredResults);
        } else {
            res.json(results);
        }

    } catch (error) {
        console.error(error);
        res.status(500).send('Hubo un error al procesar tu solicitud');
    }
});

/**
 * @swagger
 * /file/{name}:
 *  get:
 *    description: Usa para obtener los datos de un archivo
 *    parameters:
 *      - name: name
 *        description: Nombre del archivo
 *        in: path
 *        required: true
 *        type: string
 *    responses:
 *      '200':
 *        description: Una respuesta exitosa
 */
router.get('/file/:name', async (req, res) => {
    try {
        const fileName = req.params.name;
        const fileData = await apiExternal.getFileData(fileName);
        res.json(fileData);
    } catch (error) {
        console.error(error);
        res.status(500).send('Hubo un error al procesar tu solicitud');
    }
});

module.exports = router;