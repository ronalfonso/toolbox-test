const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const apiExternal = require('../external-apis/tbxnet');

// Crea una instancia de MockAdapter para simular las respuestas de axios
const mock = new MockAdapter(axios);

describe('tbxnet', () => {
    afterEach(() => {
        // Restablece todos los controladores de ruta simulados después de cada prueba
        mock.reset();
    });

    test('getFiles', async () => {
        // Simula una respuesta para la solicitud getFiles
        mock.onGet('https://echo-serv.tbxnet.com/v1/secret/files').reply(200, {
            files: ['file1.csv', 'file2.csv']
        });

        const files = await apiExternal.getFiles();

        // Verifica que getFiles devuelve los archivos correctos
        expect(files).toEqual(['file1.csv', 'file2.csv']);
    });


});
