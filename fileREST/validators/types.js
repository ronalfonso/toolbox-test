// Función para verificar si un valor no está vacío
function isNotEmpty(value) {
    return value && value.trim() !== '';
}

// Función para verificar si un valor es un número
function isNumber(value) {
    return !isNaN(value);
}

// Función para verificar si un valor es un hexadecimal de 32 dígitos
function is32DigitHex(value) {
    return /^[0-9a-fA-F]{32}$/.test(value);
}

module.exports = { isNumber, is32DigitHex, isNotEmpty };
