import './App.scss';
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {startGetFiles} from "./store/files";
import {Button, Container, FloatingLabel, Navbar, Table} from "react-bootstrap";
import Form from 'react-bootstrap/Form';

function App() {
  const dispatch = useDispatch();
  const {fileList} = useSelector((state) => state.files);
  const [files, setFiles] = useState([]);
  const [tableTitle] = useState(['File Name', 'Text', 'Number', 'Hex', 'Status']);
  const [searchTerm, setSearchTerm] = useState('');

  const _getFileList = () => {
    setSearchTerm('');
    dispatch(startGetFiles());
  }

  const doListFiles = (files) => {
    const data = [];
    files.forEach( (file) => {
      let fileObj = {};
      const now = new Date();
      if (file.lines.length > 0) {
        file.lines.forEach( (line) => {
          fileObj.id = now.getTime();
          fileObj.name = file.file;
          fileObj.text = line.text;
          fileObj.number = line.number;
          fileObj.hex = line.hex;
          data.push(fileObj);
        })
      } else {
        fileObj.id = now.getTime();
        fileObj.name = file.file;
        fileObj.lines = file.lines.length;

        if (file.error) fileObj.error = file.error;

        data.push(fileObj);
      }
    })

    setFiles(data);
  }

  const getStatus = (file) => {
    if (file.error) return 'Error'

    if (file.lines === 0) return 'Archivo con errores'

    return 'Ok'
  }

  const searchFile = () => {
    dispatch(startGetFiles(searchTerm))
  }

  const cleanSearch = () => {
    _getFileList();
  }

  useEffect(() => {
    _getFileList();
  }, []);

  useEffect(() => {
    if (fileList.length > 0) {
      doListFiles(fileList)
    }
  }, [fileList]);

  return (
    <div className="main">
      <Navbar expand="lg" className="bg-body-secondary navbar_body ">
        <Container>
          <Navbar.Brand className="text-light">React-Test App</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
        </Container>
      </Navbar>

      <div className="content">
        <div className="search">
          <FloatingLabel
              controlId="floatingInput"
              label="Nombre del archivo"
              className="input_search"
              onChange={(e) => {
                setSearchTerm(e.target.value)
              }}
          >
            <Form.Control type="text" placeholder="Nombre del archivo" />
          </FloatingLabel>
          <Button variant="outline-primary" onClick={searchFile}>Buscar</Button>
          <Button variant="outline-danger" onClick={cleanSearch}>Limpiar</Button>
        </div>

        <div className="table_container">
          <Table striped bordered hover>
            <thead>
            <tr>
              {
                tableTitle.map((title, index) => <th key={index}>{title}</th>)
              }
            </tr>
            </thead>
            <tbody className={"table_body"} style={{height: '450px'}}>
            {
              files.length > 0 ?
                  files.map( (file, index) => (
                      <tr key={index + 1}>
                        <td>{file.name}</td>
                        <td>{file.text}</td>
                        <td>{file.number}</td>
                        <td>{file.hex}</td>
                        <td>{getStatus(file)}</td>
                      </tr>
                  ))
                  :
                  <span>No se encontraron resultados</span>
            }

            </tbody>
          </Table>
        </div>
      </div>



    </div>
  );
}

export default App;
