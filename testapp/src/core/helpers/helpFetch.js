
export const helpFetch = () => {
    const customFetch = (url, options) => {

        const defaultHeader = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
        };


        options.method = options.method || 'GET';
        options.headers = options.headers ? {...options, ...defaultHeader} : defaultHeader;
        options.mode = 'cors';

        if (options.headers.headers) {
            Object.assign(options.headers, options.headers.headers);
            delete options.headers.headers;
        }

        options.body = JSON.stringify(options.data) || false;
        delete options.data
        if (options.method === 'GET') delete options.body;

        let status = null;
        let statusText = null;
        let error = null;
        return fetch(url, options)
            .then(resp => {
                status = resp.status;
                statusText = resp.statusText.toUpperCase().replace(' ', '_');
                error = !resp.ok;
                return resp.json();
            })
            .then(data => {
                if (status >= 200 && status < 300 && data.status >= 200 && data.status < 300) {
                    return {
                        data,
                        status,
                        statusText,
                        error,
                        message: data.message
                    };
                } else {
                    if (data.status > 400) {
                        status = data.status
                        error = data.error
                        statusText = data.statusText
                    }
                    return {
                        data: data,
                        status: status || 0,
                        statusText: statusText || 'Ocurrió un error',
                        error: error,
                        message: data.message || data.error
                    };
                }
            })
            .catch(err => {
                console.log(err);
            })
    }
    const get = (url, options = {}) => customFetch(url, options);
    const post = (url, options = {}) => {
        options.method = 'POST'
        return customFetch(url, options)
    }
    const put = (url, options = {}) => {
        options.method = 'PUT'
        return customFetch(url, options)
    }
    const del = (url, options = {}) => {
        options.method = 'DELETE'
        return customFetch(url, options)
    }

    return {
        get, post, put, del,
    };
}