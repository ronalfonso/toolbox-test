import {createSlice} from "@reduxjs/toolkit";


export const filesSlice = createSlice({
    name: 'files',
    initialState: {
        fileList: []
    },
    reducers: {
        fileList: (state, {payload}) => {
            state.fileList = payload;
        }
    }
})

export const {fileList} = filesSlice.actions;

export default filesSlice.reducer;