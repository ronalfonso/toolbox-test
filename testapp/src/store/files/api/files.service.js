import {helpFetch} from "../../../core/helpers/helpFetch";


const BASE_URL = 'http://localhost:5000';
const URL_COMPONENT = `${BASE_URL}/files`

export const getFileList = async (fileName = null) => {
    let url = `${URL_COMPONENT}/data`;
    if (fileName !== null) url = `${url}?fileName=${fileName}`;
    return helpFetch().get(url).then(resp => {
        return resp;
    })
        .catch(err => console.log(err));
}