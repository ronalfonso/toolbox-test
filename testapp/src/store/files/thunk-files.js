import {getFileList} from "./api/files.service";
import {fileList} from "./filesSlice";


export const startGetFiles = (fileName = null) => {
    return async (dispatch) => {
        return getFileList(fileName).then(resp => {
            if (resp.status === 200) dispatch(fileList(resp.data))

            if (resp === undefined) return [];
        })
            .catch(err => {
                console.log(err);
            })
    }
}