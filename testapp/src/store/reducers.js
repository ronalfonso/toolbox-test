import { combineReducers } from "redux";
import {filesSlice} from "./files/filesSlice";

const rootReducer = combineReducers({
    files: filesSlice.reducer,
})

export default rootReducer;