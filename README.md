# Proyecto Full Stack con Node.js, Express, React y Redux

## Descripción

Este es un proyecto Full Stack que utiliza Node.js y Express para el backend, y React con Redux para el frontend. El proyecto se puede levantar utilizando Docker Compose.

## Requisitos

- Docker
- Docker Compose
- Git

## Instrucciones de instalación

1. Clonar el repositorio

    Primero, necesitas clonar el repositorio desde GitLab. Puedes hacerlo ejecutando el siguiente comando en tu terminal:

```bash
git clone https://gitlab.com/ronalfonso/toolbox-test
```

2. Navegar hasta el directorio del proyecto

    Una vez que hayas clonado el repositorio, navega hasta el directorio del proyecto con el siguiente comando:
```bash
cd toobox-test
```

3. Levantar los servicios con Docker Compose

   Ahora puedes levantar los servicios (backend y frontend) utilizando Docker Compose. Ejecuta el siguiente comando en tu terminal:

```bash
docker-compose up
```

## Uso
Una vez que los contenedores estén en ejecución, puedes acceder a la aplicación de la siguiente manera:

Frontend (React): http://localhost:3000
Backend (Express): http://localhost:5000

## Contribuir
Si deseas contribuir a este proyecto, por favor asegúrate de seguir nuestras guías de contribución y código de conducta.

## Licencia
Este proyecto está licenciado bajo los términos de la licencia MIT.

## Contacto
Si tienes alguna pregunta o comentario, por favor no dudes en contactarnos.

¡Disfruta del proyecto!

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/colored.png)](#contributors)

## Autor
[<img alt="" src="https://avatars.githubusercontent.com/u/41694372?v=4" width="100">](https://github.com/orgs/SancorSalud/people/ronalfonso)

